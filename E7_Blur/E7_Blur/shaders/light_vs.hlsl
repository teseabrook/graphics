// Light vertex shader
// Standard issue vertex shader, apply matrices, pass info to pixel shader

cbuffer MatrixBuffer : register(b0)
{
	matrix worldMatrix;
	matrix viewMatrix;
	matrix projectionMatrix;
	matrix lightViewMatrix0;
	matrix lightProjectionMatrix0;
	matrix lightViewMatrix1;
	matrix lightProjectionMatrix1;
};

struct InputType
{
	float4 position : POSITION;
	float2 tex : TEXCOORD0;
	float3 normal : NORMAL;
};

Texture2D heightMap : register(t0);
SamplerState sampler0 : register(s0);

struct OutputType
{
	float4 position : SV_POSITION;
	float2 tex : TEXCOORD0;
	float3 normal : NORMAL;
	float3 worldPosition : TEXCOORD1;
	float4 lightViewPos0 : TEXCOORD2;
	float4 lightViewPos1 : TEXCOORD3;
};

//Texture2D heightMap : register(t0);
//SamplerState sampler0 : register(s0);

float4 deform(InputType input, float4 transformedPosition)
{
	//Deform according to a given heightmap. Currently just using the R channel
	float4 heightMapVal = heightMap.SampleLevel(sampler0, input.tex, 0);
	float4 deformedVal = transformedPosition;
	deformedVal.y += heightMapVal.r * 10;
	return deformedVal;
	//return float4(0.0f, 0.0f, 0.0f, 0.0f);

}

OutputType main(InputType input)
{
	OutputType output;

	// Calculate the position of the vertex against the world, view, and projection matrices.
	output.position = mul(input.position, worldMatrix);
	output.position = mul(output.position, viewMatrix);
	output.position = mul(output.position, projectionMatrix);

	output.position += deform(input, output.position);

	// Store the texture coordinates for the pixel shader.
	output.tex = input.tex;

	output.lightViewPos0 = mul(input.position, worldMatrix);
	output.lightViewPos0 = mul(output.lightViewPos0, lightViewMatrix0);
	output.lightViewPos0 = mul(output.lightViewPos0, lightProjectionMatrix0);

	output.lightViewPos1 = mul(input.position, worldMatrix);
	output.lightViewPos1 = mul(output.lightViewPos1, lightViewMatrix1);
	output.lightViewPos1 = mul(output.lightViewPos1, lightProjectionMatrix1);

	// Calculate the normal vector against the world matrix only and normalise.
	output.normal = mul(input.normal, (float3x3)worldMatrix);
	output.normal = normalize(output.normal);
	output.worldPosition = mul(input.position, worldMatrix).xyz;

	return output;
}