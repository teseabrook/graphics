// Light pixel shader
// Calculate diffuse lighting for a single directional light (also texturing)

Texture2D texture0 : register(t0);
Texture2D depth0 : register(t1);
Texture2D depth1 : register(t2);

SamplerState sampler0 : register(s0);
SamplerState shadowSampler : register(s1);

cbuffer LightBuffer : register(b0)
{
	float4 diffuseColour;
	float3 lightDirection;
	float3 lightPosition;
	float2 padding;
};

cbuffer LightBuffer1 : register(b1)
{
	float4 diffuseColour1;
	float3 lightDirection1;
	float3 lightPosition1;
	float2 padding1;
};

struct InputType
{
	float4 position : SV_POSITION;
	float2 tex : TEXCOORD0;
	float3 normal : NORMAL;
	float3 worldPosition : TEXCOORD1;
	float4 lightViewPos0 : TEXCOORD2;
	float4 lightViewPos1 : TEXCOORD3;
};

// Calculate lighting intensity based on direction and normal. Combine with light colour.
float4 calculateLighting(float3 lightDirection, float3 normal, float4 diffuse)
{
	float intensity = saturate(dot(normal, lightDirection));
	float4 colour = saturate(diffuse * intensity);
	return colour;
}

float4 main(InputType input) : SV_TARGET
{
	float4 textureColour;
	float4 lightColour;
	float4 lightColour1;

	float depthValue;
	float lightDepthValue;
	float shadowMapBias = 0.005f;

	// Calculate the projected texture coordinates.
	float2 pTexCoord0 = input.lightViewPos0.xy / input.lightViewPos0.w;
	pTexCoord0 *= float2(0.5, -0.5);
	pTexCoord0 += float2(0.5f, 0.5f);

	// Calculate the projected texture coordinates.
	float2 pTexCoord1 = input.lightViewPos1.xy / input.lightViewPos1.w;
	pTexCoord1 *= float2(0.5, -0.5);
	pTexCoord1 += float2(0.5f, 0.5f);

	// Determine if the projected coordinates are in the 0 to 1 range.  If not don't do lighting.
	if (pTexCoord0.x < 0.f || pTexCoord0.x > 1.f || pTexCoord0.y < 0.f || pTexCoord0.y > 1.f || pTexCoord1.x < 0.f || pTexCoord1.x > 1.f || pTexCoord1.y < 0.f || pTexCoord1.y > 1.f )
	{
		//return textureColour;
	}

	// Sample the texture. Calculate light intensity and colour, return light*texture for final pixel colour.
	textureColour = texture0.Sample(sampler0, input.tex);
	lightColour = calculateLighting(-lightDirection, input.normal, diffuseColour);
	lightColour1 = calculateLighting(-lightDirection1, input.normal, diffuseColour1);
	
	//Calculate the angle between the vector between lightPos and pixelPos, and get the angle between that and lightDir
	//Compare with maxangle to see if it's in the cone
	float cAngle = cos(3.14159 / 8.0);
	float pAngle = dot(normalize(input.worldPosition - lightPosition.xyz), normalize(lightDirection));

	float4 spotColour;

	if (pAngle < cAngle)
	{
		//Work out if you're in shadow for light 1
		/*depthValue = depth0.Sample(shadowSampler, pTexCoord0).r;
		// Calculate the depth from the light.
		lightDepthValue = input.lightViewPos0.z / input.lightViewPos0.w;
		lightDepthValue -= shadowMapBias;

		if (lightDepthValue < depthValue)
		{
			lightColour = calculateLighting(-lightDirection, input.normal, diffuseColour);
			//spotColour = (1.0f, 1.0f, 1.0f, 1.0f);
		}
		else
		{
			lightColour = (0.01f, 0.01f, 0.01f, 1.0f);
			spotColour = (0.01f, 0.01f, 0.01f, 1.0f);
		}*/



		spotColour = lightColour * textureColour;
	}
	else
	{
		lightColour = (0.01f, 0.01f, 0.01f, 1.0f);
		spotColour = lightColour * textureColour;
	}

	float cAngle0 = cos(3.14159 / 4.0);
	float pAngle0 = dot(normalize(input.worldPosition - lightPosition1.xyz), normalize(lightDirection1));

	float4 spotColour1;

	if (pAngle0 < cAngle0)
	{	
		//Work out if you're in shadow for light 1
		/*depthValue = depth1.Sample(shadowSampler, pTexCoord1).r;
		// Calculate the depth from the light.
		lightDepthValue = input.lightViewPos1.z / input.lightViewPos1.w;
		lightDepthValue -= shadowMapBias;

		if (lightDepthValue > depthValue)
		{
			lightColour1 = (0.01f, 0.01f, 0.01f, 1.0f);
		}*/
		
		spotColour1 = lightColour1 * textureColour;
	}
	else
	{
		lightColour1 = (0.01f, 0.01f, 0.01f, 1.0f);
		spotColour1 = lightColour1 * textureColour;
	}

	return spotColour + spotColour1;

	//return lightColour;

	//return textureColour;
}



