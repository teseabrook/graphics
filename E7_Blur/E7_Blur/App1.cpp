#include "App1.h"

App1::App1()
{

}

void App1::init(HINSTANCE hinstance, HWND hwnd, int screenWidth, int screenHeight, Input *in, bool VSYNC, bool FULL_SCREEN)
{
	// Call super/parent init function (required!)
	BaseApplication::init(hinstance, hwnd, screenWidth, screenHeight, in, VSYNC, FULL_SCREEN);

	//Load textures & height maps
	textureMgr->loadTexture(L"seafloor", L"res/sand.png");
	textureMgr->loadTexture(L"heightMap", L"res/heighmap.png");
	textureMgr->loadTexture(L"flatMap", L"res/height.png");
	textureMgr->loadTexture(L"sun", L"res/sun.png");
	textureMgr->loadTexture(L"fish", L"res/fish.jpg");
	textureMgr->loadTexture(L"yellow", L"res/yellow.png");
	textureMgr->loadTexture(L"brick", L"res/brick1.dds");
	
	//Initialise meshes
	cubeMesh = new CubeMesh(renderer->getDevice(), renderer->getDeviceContext());
	orthoMesh = new OrthoMesh(renderer->getDevice(), renderer->getDeviceContext(), screenWidth, screenHeight);	// Full screen size
	plane = new PlaneMesh(renderer->getDevice(), renderer->getDeviceContext(), 100);
	lightS1 = new SphereMesh(renderer->getDevice(), renderer->getDeviceContext(), 10);
	lightS2 = new SphereMesh(renderer->getDevice(), renderer->getDeviceContext(), 10);
	shadowSphere = new SphereMesh(renderer->getDevice(), renderer->getDeviceContext(), 80);
	manipulateMesh = new PlaneMesh(renderer->getDevice(), renderer->getDeviceContext(), 25);

	minimap = new OrthoMesh(renderer->getDevice(), renderer->getDeviceContext(), 300, 300);

	shadows[0] = new SphereMesh(renderer->getDevice(), renderer->getDeviceContext(), 80);
	shadows[1] = new SphereMesh(renderer->getDevice(), renderer->getDeviceContext(), 80);

	playerIndicator = new SphereMesh(renderer->getDevice(), renderer->getDeviceContext());

	//Initialise shaders
	lightShader = new LightShader(renderer->getDevice(), hwnd);
	textureShader = new TextureShader(renderer->getDevice(), hwnd);
	horizontalBlurShader = new HorizontalBlurShader(renderer->getDevice(), hwnd);
	verticalBlurShader = new VerticalBlurShader(renderer->getDevice(), hwnd);
	depthShader = new DepthShader(renderer->getDevice(), hwnd);

	//Postproccessing textures
	renderTexture = new RenderTexture(renderer->getDevice(), screenWidth, screenHeight, SCREEN_NEAR, SCREEN_DEPTH);
	horizontalBlurTexture = new RenderTexture(renderer->getDevice(), screenWidth, screenHeight, SCREEN_NEAR, SCREEN_DEPTH);
	verticalBlurTexture = new RenderTexture(renderer->getDevice(), screenWidth, screenHeight, SCREEN_NEAR, SCREEN_DEPTH);
	depthTexture[0] = new ShadowMap(renderer->getDevice(), 1024, 1024);
	depthTexture[1] = new ShadowMap(renderer->getDevice(), 1024, 1024);
	minimapTexture = new RenderTexture(renderer->getDevice(), 1024, 1024, SCREEN_NEAR, SCREEN_DEPTH);

	//Define 2 lights
	light = new Light;
	light->setAmbientColour(0.0f, 0.0f, 0.0f, 1.0f);
	light->setDiffuseColour(1.0f, 1.0f, 1.0f, 1.0f);
	light->setDirection(0.0f, -0.2f, 0.2f);
	light->setPosition(55.0f, 45.0f, 75.0f); 
	light->generateViewMatrix();
	light->generateProjectionMatrix(SCREEN_NEAR, SCREEN_DEPTH);
	light->generateOrthoMatrix((float)screenWidth, (float)screenHeight, 0.1f, 100.f);

	light1 = new Light;
	light1->setDiffuseColour(0.25f, 0.25f, 0.25f, 1.0f);
	light1->setDirection(0.0f, -1.0f, 0.0f);
	light1->setPosition(80.0f, 30.0f, 50.0f);
	light1->generateViewMatrix();
	light1->generateProjectionMatrix(SCREEN_NEAR, SCREEN_DEPTH);
	light1->generateOrthoMatrix((float)screenWidth, (float)screenHeight, 0.1f, 100.f);

}


App1::~App1()
{
	// Run base application deconstructor
	BaseApplication::~BaseApplication();

	// Release the Direct3D object.

}


bool App1::frame()
{
	bool result;

	//light->setPosition(lightPos[0], lightPos[1], lightPos[2]);
	//light->setDirection(lightDir[0], lightDir[1], lightDir[2]);
		//light->setDirection(0.0f, -0.2f, 0.2f);
	//light->setPosition(55.0f, 45.0f, 75.0f);

	//camera->setPosition(50, 100, 50);

	result = BaseApplication::frame();
	if (!result)
	{
		return false;
	}
	
	// Render the graphics.
	result = render();
	if (!result)
	{
		return false;
	}

	return true;
}

bool App1::render()
{
	//Depth Pass
	//depthPass(0, light);
	//depthPass(1, light1);

	// Render scene
	minimapPass();
	firstPass();
	// Apply horizontal blur stage
	horizontalBlur();
	// Apply vertical blur to the horizontal blur stage
	verticalBlur();
	// Render final pass to frame buffer
	finalPass();

	return true;
}

void App1::depthPass(int lightNo, Light* lightA)
{
	//Set the render target
	depthTexture[lightNo]->BindDsvAndSetNullRenderTarget(renderer->getDeviceContext());

	// get the world, view, and projection matrices from the camera and d3d objects.
	//light->generateViewMatrix();
	XMMATRIX lightViewMatrix = lightA->getViewMatrix();
	XMMATRIX lightProjectionMatrix = lightA->getProjectionMatrix();
	XMMATRIX worldMatrix = renderer->getWorldMatrix();

	plane->sendData(renderer->getDeviceContext());
	depthShader->setShaderParameters(renderer->getDeviceContext(), worldMatrix, lightViewMatrix, lightProjectionMatrix, textureMgr->getTexture(L"heightMap"));
	depthShader->render(renderer->getDeviceContext(), plane->getIndexCount());


	//Transform the sphere to be at the light position (one unit up in y position so it doesnt interfere with shadows)
	worldMatrix = XMMatrixTranslation(55.0f, 46.0f, 75.0f);

	//Render the first sphere
	lightS1->sendData(renderer->getDeviceContext());
	depthShader->setShaderParameters(renderer->getDeviceContext(), worldMatrix, lightViewMatrix, lightProjectionMatrix, textureMgr->getTexture(L"flatMap"));
	depthShader->render(renderer->getDeviceContext(), lightS1->getIndexCount());

	//Reset the world matrix & set it to the next position
	//I feel like it looks in the wrong position but whatever
	worldMatrix = renderer->getWorldMatrix();
	worldMatrix = XMMatrixTranslation(80.0f, 31.0f, 50.0f);

	//Render the second sphere
	lightS2->sendData(renderer->getDeviceContext());
	depthShader->setShaderParameters(renderer->getDeviceContext(), worldMatrix, lightViewMatrix, lightProjectionMatrix, textureMgr->getTexture(L"flatMap"));
	depthShader->render(renderer->getDeviceContext(), lightS2->getIndexCount());

	//Reset the matrix again and move the third sphere to the correct place
	//This sphere should be in a position to be lit by both lights to demonstrate shadows
	worldMatrix = renderer->getWorldMatrix();
	worldMatrix = XMMatrixMultiply(XMMatrixScaling(2.0f, 2.0f, 2.0f), XMMatrixTranslation(40.0f, 10.0f, 35.0f));

	shadowSphere->sendData(renderer->getDeviceContext());
	depthShader->setShaderParameters(renderer->getDeviceContext(), worldMatrix, lightViewMatrix, lightProjectionMatrix, textureMgr->getTexture(L"flatMap"));
	depthShader->render(renderer->getDeviceContext(), shadowSphere->getIndexCount());

	

	// Set back buffer as render target and reset view port.
	renderer->setBackBufferRenderTarget();
	renderer->resetViewport();
}

void App1::minimapPass()
{
	// Set the render target to be the render to texture and clear it
	minimapTexture->setRenderTarget(renderer->getDeviceContext());
	minimapTexture->clearRenderTarget(renderer->getDeviceContext(), 0.0f, 0.0f, 0.0f, 1.0f);

	//Store the camera's position, then set it to the new positon
	XMFLOAT3 positionStore = camera->getPosition();
	XMFLOAT3 rotationStore = camera->getRotation();

	camera->setPosition(50, 100, 50);
	camera->setRotation(90, 0, 90);

	//Render the scene, ignoring lighting and shadows
	// Get matrices
	camera->update();
	XMMATRIX worldMatrix = renderer->getWorldMatrix();
	XMMATRIX viewMatrix = camera->getViewMatrix();
	XMMATRIX projectionMatrix = renderer->getProjectionMatrix();

	plane->sendData(renderer->getDeviceContext());
	textureShader->setShaderParameters(renderer->getDeviceContext(), worldMatrix, viewMatrix, projectionMatrix, textureMgr->getTexture(L"seafloor"));
	textureShader->render(renderer->getDeviceContext(), plane->getIndexCount());


	worldMatrix = XMMatrixTranslation(55.0f, 46.0f, 75.0f);
	lightS1->sendData(renderer->getDeviceContext());
	textureShader->setShaderParameters(renderer->getDeviceContext(), worldMatrix, viewMatrix, projectionMatrix, textureMgr->getTexture(L"sun"));
	textureShader->render(renderer->getDeviceContext(), lightS1->getIndexCount());

	worldMatrix = renderer->getWorldMatrix();
	worldMatrix = XMMatrixTranslation(80.0f, 31.0f, 50.0f);
	lightS2->sendData(renderer->getDeviceContext());
	textureShader->setShaderParameters(renderer->getDeviceContext(), worldMatrix, viewMatrix, projectionMatrix, textureMgr->getTexture(L"sun"));
	textureShader->render(renderer->getDeviceContext(), lightS2->getIndexCount());

	worldMatrix = renderer->getWorldMatrix();
	worldMatrix = XMMatrixMultiply(XMMatrixScaling(2.0f, 2.0f, 2.0f), XMMatrixTranslation(40.0f, 10.0f, 60.0f));
	shadowSphere->sendData(renderer->getDeviceContext());
	textureShader->setShaderParameters(renderer->getDeviceContext(), worldMatrix, viewMatrix, projectionMatrix, textureMgr->getTexture(L"fish"));
	textureShader->render(renderer->getDeviceContext(), shadowSphere->getIndexCount());

	worldMatrix = renderer->getWorldMatrix();
	worldMatrix = XMMatrixMultiply(XMMatrixScaling(5.0f, 5.0f, 5.0f), XMMatrixTranslation(positionStore.x + 10, positionStore.y, positionStore.z - 5));
	playerIndicator->sendData(renderer->getDeviceContext());
	textureShader->setShaderParameters(renderer->getDeviceContext(), worldMatrix, viewMatrix, projectionMatrix, textureMgr->getTexture(L"yellow"));
	textureShader->render(renderer->getDeviceContext(), playerIndicator->getIndexCount());

	worldMatrix = renderer->getWorldMatrix();
	worldMatrix = XMMatrixTranslation(0, 10, 100);
	manipulateMesh->sendData(renderer->getDeviceContext());
	textureShader->setShaderParameters(renderer->getDeviceContext(), worldMatrix, viewMatrix, projectionMatrix, textureMgr->getTexture(L"brick"));
	textureShader->render(renderer->getDeviceContext(), manipulateMesh->getIndexCount());


	//Reset everything
	camera->setPosition(positionStore.x, positionStore.y, positionStore.z);
	camera->setRotation(rotationStore.x, rotationStore.y, rotationStore.z);
	renderer->setBackBufferRenderTarget();

}

void App1::firstPass()
{
	// Set the render target to be the render to texture and clear it
	renderTexture->setRenderTarget(renderer->getDeviceContext());
	renderTexture->clearRenderTarget(renderer->getDeviceContext(), 0.0f, 0.0f, 1.0f, 1.0f);

	// Get matrices
	camera->update();
	XMMATRIX worldMatrix = renderer->getWorldMatrix();
	XMMATRIX viewMatrix = camera->getViewMatrix();
	XMMATRIX projectionMatrix = renderer->getProjectionMatrix();

	// Render shape with simple lighting shader set.
	//cubeMesh->sendData(renderer->getDeviceContext());
	//lightShader->setShaderParameters(renderer->getDeviceContext(), worldMatrix, viewMatrix, projectionMatrix, textureMgr->getTexture(L"brick"), light);
	//lightShader->render(renderer->getDeviceContext(), cubeMesh->getIndexCount());

	plane->sendData(renderer->getDeviceContext());
	lightShader->setShaderParameters(renderer->getDeviceContext(), worldMatrix, viewMatrix, projectionMatrix, light->getViewMatrix(), light->getProjectionMatrix(), light1->getViewMatrix(), light1->getProjectionMatrix(), textureMgr->getTexture(L"seafloor"), textureMgr->getTexture(L"flatMap"), light, light1, depthTexture[0]->getDepthMapSRV(), depthTexture[1]->getDepthMapSRV());
	lightShader->render(renderer->getDeviceContext(), plane->getIndexCount());


	//Transform the sphere to be at the light position (one unit up in y position so it doesnt interfere with shadows)
	worldMatrix = XMMatrixTranslation(55.0f, 46.0f, 75.0f);

	//Render the first sphere
	lightS1->sendData(renderer->getDeviceContext());
	lightShader->setShaderParameters(renderer->getDeviceContext(), worldMatrix, viewMatrix, projectionMatrix, light->getViewMatrix(), light->getProjectionMatrix(), light1->getViewMatrix(), light1->getProjectionMatrix(), textureMgr->getTexture(L"sun"), textureMgr->getTexture(L"flatMap"), light, light1, depthTexture[0]->getDepthMapSRV(), depthTexture[1]->getDepthMapSRV(), false);
	lightShader->render(renderer->getDeviceContext(), lightS1->getIndexCount());

	//Reset the world matrix & set it to the next position
	//I feel like it looks in the wrong position but whatever
	worldMatrix = renderer->getWorldMatrix();
	worldMatrix = XMMatrixTranslation(80.0f, 31.0f, 50.0f);

	//Render the second sphere
	lightS2->sendData(renderer->getDeviceContext());
	lightShader->setShaderParameters(renderer->getDeviceContext(), worldMatrix, viewMatrix, projectionMatrix, light->getViewMatrix(), light->getProjectionMatrix(), light1->getViewMatrix(), light1->getProjectionMatrix(), textureMgr->getTexture(L"sun"), textureMgr->getTexture(L"flatMap"), light, light1, depthTexture[0]->getDepthMapSRV(), depthTexture[1]->getDepthMapSRV(), false);
	lightShader->render(renderer->getDeviceContext(), lightS2->getIndexCount());

	//Reset the matrix again and move the third sphere to the correct place
	//This sphere should be in a position to be lit by both lights to demonstrate shadows
	worldMatrix = renderer->getWorldMatrix();
	worldMatrix = XMMatrixMultiply(XMMatrixScaling(2.0f, 2.0f, 2.0f), XMMatrixTranslation(40.0f, 10.0f, 60.0f));

	shadowSphere->sendData(renderer->getDeviceContext());
	lightShader->setShaderParameters(renderer->getDeviceContext(), worldMatrix, viewMatrix, projectionMatrix, light->getViewMatrix(), light->getProjectionMatrix(), light1->getViewMatrix(), light1->getProjectionMatrix(), textureMgr->getTexture(L"fish"), textureMgr->getTexture(L"flatMap"), light, light1, depthTexture[0]->getDepthMapSRV(), depthTexture[1]->getDepthMapSRV(), false);
	lightShader->render(renderer->getDeviceContext(), shadowSphere->getIndexCount());


	//Now, calculate where the shadows should be
	//We need the vector between the light and the sphere, and then extrapolate that to the y level of the sea floor
	//Then, deform this according to a far smaller version of the heightmap and we're golden
	//Since the shadows are flattened spheres, they need a bit of correction

	worldMatrix = renderer->getWorldMatrix();
	XMFLOAT3 pos0 = calculatePosition(XMFLOAT3(40.0f, 10.0f, 60.0f), light->getPosition(), 0.25);
	worldMatrix = XMMatrixMultiply(XMMatrixScaling(2.0f, 0.0f, 2.0f), XMMatrixTranslation(pos0.x + 5, 0.25f, pos0.z));

	shadows[0]->sendData(renderer->getDeviceContext());
	lightShader->setShaderParameters(renderer->getDeviceContext(), worldMatrix, viewMatrix, projectionMatrix, light->getViewMatrix(), light->getProjectionMatrix(), light1->getViewMatrix(), light1->getProjectionMatrix(), textureMgr->getTexture(L"flatMap"), textureMgr->getTexture(L"flatMap"), light, light1, depthTexture[0]->getDepthMapSRV(), depthTexture[1]->getDepthMapSRV(), false);
	lightShader->render(renderer->getDeviceContext(), shadows[0]->getIndexCount());

	/*worldMatrix = renderer->getWorldMatrix();
	XMFLOAT3 pos1 = calculatePosition(XMFLOAT3(40.0f, 10.0f, 60.0f), light1->getPosition(), 0.25);
	worldMatrix = XMMatrixMultiply(XMMatrixScaling(2.0f, 0.0f, 2.0f), XMMatrixTranslation(pos1.x - 5, 0.25f, pos1.z));

	shadows[1]->sendData(renderer->getDeviceContext());
	lightShader->setShaderParameters(renderer->getDeviceContext(), worldMatrix, viewMatrix, projectionMatrix, light1->getViewMatrix(), light1->getProjectionMatrix(), light1->getViewMatrix(), light1->getProjectionMatrix(), textureMgr->getTexture(L"flatMap"), textureMgr->getTexture(L"flatMap"), light, light1, depthTexture[0]->getDepthMapSRV(), depthTexture[1]->getDepthMapSRV(), false);
	lightShader->render(renderer->getDeviceContext(), shadows[1]->getIndexCount());*/

	worldMatrix = renderer->getWorldMatrix();
	worldMatrix = XMMatrixTranslation(0, 10, 100);
	manipulateMesh->sendData(renderer->getDeviceContext());
	lightShader->setShaderParameters(renderer->getDeviceContext(), worldMatrix, viewMatrix, projectionMatrix, light1->getViewMatrix(), light1->getProjectionMatrix(), light1->getViewMatrix(), light1->getProjectionMatrix(), textureMgr->getTexture(L"brick"), textureMgr->getTexture(L"heightMap"), light, light1, depthTexture[0]->getDepthMapSRV(), depthTexture[1]->getDepthMapSRV(), false);
	lightShader->render(renderer->getDeviceContext(), manipulateMesh->getIndexCount());

	// Reset the render target back to the original back buffer and not the render to texture anymore.
	renderer->setBackBufferRenderTarget();
}

XMFLOAT3 App1::calculatePosition(XMFLOAT3 position, XMFLOAT3 lightPosition, int targetY)
{
	if (position.y > lightPosition.y)
	{
		//Error check
		return XMFLOAT3(0, 0, 0);
	}

	else
	{
		//Calculate the vector
		XMFLOAT3 v = XMFLOAT3(position.x - lightPosition.x, position.y - lightPosition.y, position.z - lightPosition.z);
		//Normalise the vector
		
		XMFLOAT3 u;
		//By dividing through by y (such that v.y becomes 1, then multiplying by the y difference, that should give the vector
		u.x = v.y / v.y;
		u.y = v.y / v.y;
		u.z = v.z / v.y;

		u.x *= (position.y - targetY);
		u.y *= (position.y - targetY);
		u.z *= (position.y - targetY);

		//The vector from position to the shadow position is now known
	

		//XMFLOAT3 s = XMFLOAT3(u.x - position.x, u.y - position.y, u.z - position.z);
		XMFLOAT3 s = XMFLOAT3(position.x - u.x, position.y - u.x, position.z - u.z);

		return s;


	}

}

void App1::horizontalBlur()
{
	XMMATRIX worldMatrix, baseViewMatrix, orthoMatrix;

	float screenSizeX = (float)horizontalBlurTexture->getTextureWidth();
	horizontalBlurTexture->setRenderTarget(renderer->getDeviceContext());
	horizontalBlurTexture->clearRenderTarget(renderer->getDeviceContext(), 1.0f, 1.0f, 0.0f, 1.0f);

	worldMatrix = renderer->getWorldMatrix();
	baseViewMatrix = camera->getOrthoViewMatrix();
	orthoMatrix = horizontalBlurTexture->getOrthoMatrix();

	// Render for Horizontal Blur
	renderer->setZBuffer(false);
	orthoMesh->sendData(renderer->getDeviceContext());
	horizontalBlurShader->setShaderParameters(renderer->getDeviceContext(), worldMatrix, baseViewMatrix, orthoMatrix, renderTexture->getShaderResourceView(), screenSizeX);
	horizontalBlurShader->render(renderer->getDeviceContext(), orthoMesh->getIndexCount());
	renderer->setZBuffer(true);

	// Reset the render target back to the original back buffer and not the render to texture anymore.
	renderer->setBackBufferRenderTarget();
}

void App1::verticalBlur()
{
	XMMATRIX worldMatrix, baseViewMatrix, orthoMatrix;

	float screenSizeY = (float)verticalBlurTexture->getTextureHeight();
	verticalBlurTexture->setRenderTarget(renderer->getDeviceContext());
	verticalBlurTexture->clearRenderTarget(renderer->getDeviceContext(), 0.0f, 1.0f, 1.0f, 1.0f);

	worldMatrix = renderer->getWorldMatrix();
	baseViewMatrix = camera->getOrthoViewMatrix();
	// Get the ortho matrix from the render to texture since texture has different dimensions being that it is smaller.
	orthoMatrix = verticalBlurTexture->getOrthoMatrix();

	// Render for Vertical Blur
	renderer->setZBuffer(false);
	orthoMesh->sendData(renderer->getDeviceContext());
	verticalBlurShader->setShaderParameters(renderer->getDeviceContext(), worldMatrix, baseViewMatrix, orthoMatrix, horizontalBlurTexture->getShaderResourceView(), screenSizeY);
	verticalBlurShader->render(renderer->getDeviceContext(), orthoMesh->getIndexCount());
	renderer->setZBuffer(true);

	// Reset the render target back to the original back buffer and not the render to texture anymore.
	renderer->setBackBufferRenderTarget();
}

void App1::finalPass()
{
	// Clear the scene. (default blue colour)
	renderer->beginScene(0.39f, 0.58f, 0.92f, 1.0f);

	// RENDER THE RENDER TEXTURE SCENE
	// Requires 2D rendering and an ortho mesh.
	renderer->setZBuffer(false);
	XMMATRIX worldMatrix = renderer->getWorldMatrix();
	XMMATRIX orthoMatrix = renderer->getOrthoMatrix();  // ortho matrix for 2D rendering
	XMMATRIX orthoViewMatrix = camera->getOrthoViewMatrix();	// Default camera position for orthographic rendering

	orthoMesh->sendData(renderer->getDeviceContext());
	textureShader->setShaderParameters(renderer->getDeviceContext(), worldMatrix, orthoViewMatrix, orthoMatrix, verticalBlurTexture->getShaderResourceView());
	//textureShader->setShaderParameters(renderer->getDeviceContext(), worldMatrix, orthoViewMatrix, orthoMatrix, depthTexture[0]->getDepthMapSRV());
	textureShader->render(renderer->getDeviceContext(), orthoMesh->getIndexCount());
	//renderer->setZBuffer(true);

	minimap->sendData(renderer->getDeviceContext());
	worldMatrix = XMMatrixMultiply(XMMatrixRotationRollPitchYaw(0, 0, 3.14159/2), XMMatrixTranslation(-445, -190, 0));
	textureShader->setShaderParameters(renderer->getDeviceContext(), worldMatrix, orthoViewMatrix, orthoMatrix, minimapTexture->getShaderResourceView());
	//textureShader->setShaderParameters(renderer->getDeviceContext(), worldMatrix, orthoViewMatrix, orthoMatrix, depthTexture[0]->getDepthMapSRV());
	textureShader->render(renderer->getDeviceContext(), minimap->getIndexCount());
	renderer->setZBuffer(true);

	// Render GUI
	gui();

	// Present the rendered scene to the screen.
	renderer->endScene();
}

void App1::gui()
{
	// Force turn off unnecessary shader stages.
	renderer->getDeviceContext()->GSSetShader(NULL, NULL, 0);
	renderer->getDeviceContext()->HSSetShader(NULL, NULL, 0);
	renderer->getDeviceContext()->DSSetShader(NULL, NULL, 0);

	// Build UI
	ImGui::Text("FPS: %.2f", timer->getFPS());
	ImGui::Checkbox("Wireframe mode", &wireframeToggle);

	ImGui::SliderFloat3("Light Position", lightPos, 0, 100);
	ImGui::SliderFloat3("Light Direction", lightDir, -1, 1);

	ImGui::SliderFloat2("Shadow Correction", shadowCorrect, -10, 20);

	// Render UI
	ImGui::Render();
	ImGui_ImplDX11_RenderDrawData(ImGui::GetDrawData());
}

