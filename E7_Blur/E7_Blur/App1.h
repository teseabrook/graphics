// Application.h
#ifndef _APP1_H
#define _APP1_H

// Includes
#include "DXF.h"	// include dxframework
#include "LightShader.h"
#include "TextureShader.h"
#include "VerticalBlurShader.h"
#include "HorizontalBlurShader.h"
#include "DepthShader.h"

class App1 : public BaseApplication
{
public:

	App1();
	~App1();
	void init(HINSTANCE hinstance, HWND hwnd, int screenWidth, int screenHeight, Input* in, bool VSYNC, bool FULL_SCREEN);

	bool frame();

protected:
	bool render();

	//Render passes
	void depthPass(int light, Light* lightA);
	void minimapPass();
	void firstPass();
	void verticalBlur();
	void horizontalBlur();
	void finalPass();

	void gui();

	XMFLOAT3 calculatePosition(XMFLOAT3 position, XMFLOAT3 lightPosition, int yTarget);

private:
	CubeMesh* cubeMesh;
	PlaneMesh* plane;
	PlaneMesh* manipulateMesh;

	OrthoMesh* orthoMesh;
	OrthoMesh* minimap;
	//Two spheres for visual representations of the lights and one to do shadows with
	SphereMesh* lightS1;
	SphereMesh* lightS2;
	SphereMesh* shadowSphere;

	SphereMesh* shadows[2];
	SphereMesh* playerIndicator;

	LightShader* lightShader;
	TextureShader* textureShader;

	RenderTexture* renderTexture;
	RenderTexture* horizontalBlurTexture;
	RenderTexture* verticalBlurTexture;
	RenderTexture* minimapTexture;
	ShadowMap* depthTexture[2];

	VerticalBlurShader* verticalBlurShader;
	HorizontalBlurShader* horizontalBlurShader;
	DepthShader* depthShader;
	
	Light* light;
	Light* light1;
	
	float lightPos[3];
	float lightDir[3];

	float shadowCorrect[2];

};

#endif