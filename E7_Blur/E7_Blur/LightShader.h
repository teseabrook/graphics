#pragma once

#include "DXF.h"

using namespace std;
using namespace DirectX;

class LightShader : public BaseShader
{
private:
	struct LightBufferType
	{
		XMFLOAT4 diffuse;
		XMFLOAT3 direction;
		XMFLOAT3 position;
		XMFLOAT2 padding;
	};

	struct MatrixBufferType
	{
		XMMATRIX world;
		XMMATRIX view;
		XMMATRIX projection;
		XMMATRIX lightView0;
		XMMATRIX lightProjection0;
		XMMATRIX lightView1;
		XMMATRIX lightProjection1;

	};

public:
	LightShader(ID3D11Device* device, HWND hwnd);
	~LightShader();

	void setShaderParameters(ID3D11DeviceContext* deviceContext, const XMMATRIX &world, const XMMATRIX &view, const XMMATRIX &projection, const XMMATRIX &lview0, const XMMATRIX& lproj0, const XMMATRIX& lview1, const XMMATRIX& lproj1, ID3D11ShaderResourceView* texture, ID3D11ShaderResourceView* map, Light* light, Light* light1, ID3D11ShaderResourceView* shadow0, ID3D11ShaderResourceView* shadow1,  bool manipulate = true);

private:
	void initShader(const wchar_t* vs, const wchar_t* ps);

private:
	ID3D11Buffer * matrixBuffer;
	ID3D11SamplerState* sampleState;
	ID3D11SamplerState* depthSamplerState;
	ID3D11Buffer* lightBuffer;
	ID3D11Buffer* lightBuffer1;
};

