cbuffer MatrixBuffer : register(b0)
{
	matrix worldMatrix;
	matrix viewMatrix;
	matrix projectionMatrix;
};

struct InputType
{
	float4 position : POSITION;
	float2 tex : TEXCOORD0;
	float3 normal : NORMAL;
};

Texture2D heightMap : register(t0);
SamplerState sampler0 : register(s0);

struct OutputType
{
	float4 position : SV_POSITION;
	float4 depthPosition : TEXCOORD0;
};

float4 deform(InputType input, float4 transformedPosition)
{
	//Deform according to a given heightmap. Currently just using the R channel
	float4 heightMapVal = heightMap.SampleLevel(sampler0, input.tex, 0);
	float4 deformedVal = transformedPosition;
	deformedVal.y += heightMapVal.r * 10;
	return deformedVal;
	//return float4(0.0f, 0.0f, 0.0f, 0.0f);

}

OutputType main(InputType input)
{
	OutputType output;

	// Calculate the position of the vertex against the world, view, and projection matrices.
	output.position = mul(input.position, worldMatrix);
	output.position = mul(output.position, viewMatrix);
	output.position = mul(output.position, projectionMatrix);

	//Manipulate the vertices

	output.position += deform(input, output.position);


	// Store the position value in a second input value for depth value calculations.
	output.depthPosition = output.position;

	return output;
}